
n.n.n / 2014-02-25
==================

 * Added the option to modify sites and mods. Also Sites render now.
 * Minor Database changes, mainly useful for future, planned functions. Fixed the "auth" filter.
 * Cleaning and Login System.
 * Small chnages in the alert dessign. Moved the conceptual views to the normal views folder. Two views renamed for CamelCase.
 * Design Improvements.
 * Testing View functions.
 * Added dynaamic Alert rendering. Added Registration validation and saving. Added mail verifying functions. Added Account activation.
 * Backup commit.
 * Second layout preview commit
 * 21.01.14 Semiar commit.
 * Finishing the concepts. NEW EDITOR.
 * Dabug bar.
 * Added the concept-Controller for the conceptual Designs. Added Design studies for three sites. More to follow.
 * Initial Commit. Database was made. Models are ready. Basic Routing setup. Login is ready.
